const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const userRoutes = express.Router();
const PORT = 4000;

let User = require("./user.model");

app.use(cors());
app.use(bodyParser.json());

mongoose.connect("mongodb://127.0.0.1:27017/manager", {
  useNewUrlParser: true
});
const connection = mongoose.connection;

connection.once("open", function() {
  console.log("MongoDB database connection established successfully");
});

userRoutes.route("/").get(function(req, res) {
  User.find(function(err, users) {
    if (err) {
      console.log(err);
    } else {
      res.json(users);
    }
  });
});

userRoutes.route("/:id").get(function(req, res) {
  let id = req.params.id;
  User.findById(id, function(err, user) {
    res.json(user);
  });
});

// userRoutes.route('/delete/:id').delete((req, res, next) => {
//     User.findByIdAndRemove(req.params.id, (error, user) => {
//       if (error) {
//         return next(error);
//       } else {
//         res.status(200).json({
//           msg: user
//         })
//       }
//     })
//   })

userRoutes.route("/update/:id").post(function(req, res) {
  User.findById(req.params.id, function(err, user) {
    if (!user) res.status(404).send("data is not found");
    else user.user_date = req.body.user_date;
    user.user_fullname = req.body.user_fullname;
    user.user_address = req.body.user_address;
    user.user_phonenumber = req.body.user_phonenumber;
    user.user_email = req.body.user_email;
    user.user_companyname = req.body.user_companyname;
    user.user_phone = req.body.user_phone;
    user.user_website = req.body.user_website;
    user.user_socialMedia = req.body.user_socialMedia;
    user.user_completed = req.body.user_completed;

    user
      .save()
      .then(user => {
        res.json("User updated!");
      })
      .catch(err => {
        res.status(400).send("Update not possible");
      });
  });
});

userRoutes.route("/add").post(function(req, res) {
  const today = new Date();
  let user = new User(req.body);
  user
    .save()
    .then(val => {
      res.status(200).json({ user: val });
    })
    .catch(err => {
      res.status(400).send("adding new user failed");
    });
});

app.use("/manager", userRoutes);

app.listen(PORT, function() {
  console.log("Server is running on Port: " + PORT);
});
