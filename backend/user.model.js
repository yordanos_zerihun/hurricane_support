const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let User = new Schema({
  user_date: {
    type: Date,
    default: Date.now
  },
  user_fullname: {
    type: String
  },
  user_address: {
    type: String
  },
  user_phonenumber: {
    type: String
  },
  user_email: {
    type: String
  },
  user_companyname: {
    type: String
  },
  user_phone: {
    type: String
  },
  user_website: {
    type: String
  },
  user_socialMedia: {
    type: String
  },
  user_completed: {
    type: Boolean
  }
});
// let comun = new Schema({
//     customer_ID:{
//         type:User
//     }
// })

module.exports = mongoose.model("User", User);
