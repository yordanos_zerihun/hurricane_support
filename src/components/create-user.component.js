import React, { Component } from "react";
import axios from "axios";

export default class CreateUser extends Component {
  constructor(props) {
    super(props);

    this.onChangeUserFullname = this.onChangeUserFullname.bind(this);
    this.onChangeUserAddress = this.onChangeUserAddress.bind(this);
    this.onChangeUserPhonenumber = this.onChangeUserPhonenumber.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangeUserCompanyname = this.onChangeUserCompanyname.bind(this);
    this.onChangeUserPhone = this.onChangeUserPhone.bind(this);
    this.onChangeUserWebsite = this.onChangeUserWebsite.bind(this);
    this.onChangeUserSocialMedia = this.onChangeUserSocialMedia.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      user_fullname: "",
      user_address: "",
      user_phonenumber: "",
      user_email: "",
      user_companyname: "",
      user_phone: "",
      user_website: "",
      user_socialMedia: "",
      user_completed: false
    };
  }

  onChangeUserFullname(e) {
    this.setState({
      user_fullname: e.target.value
    });
  }
  onChangeUserAddress(e) {
    this.setState({
      user_address: e.target.value
    });
  }
  onChangeUserPhonenumber(e) {
    this.setState({
      user_phonenumber: e.target.value
    });
  }
  onChangeUserEmail(e) {
    this.setState({
      user_email: e.target.value
    });
  }
  onChangeUserCompanyname(e) {
    this.setState({
      user_companyname: e.target.value
    });
  }
  onChangeUserPhone(e) {
    this.setState({
      user_phone: e.target.value
    });
  }
  onChangeUserWebsite(e) {
    this.setState({
      user_website: e.target.value
    });
  }
  onChangeUserSocialMedia(e) {
    this.setState({
      user_socialMedia: e.target.value
    });
  }
  onSubmit(e) {
    e.preventDefault();

    console.log(`Form submitted:`);
    console.log(`Full name: ${this.state.user_fullname}`);
    console.log(`User address: ${this.state.user_address}`);
    console.log(`User Phone Numeber: ${this.state.user_phonenumber}`);
    console.log(`User Email: ${this.state.user_email}`);
    console.log(`User Company Name: ${this.state.user_companyname}`);
    console.log(`User Phone : ${this.state.user_phone}`);
    console.log(`User website: ${this.state.user_website}`);
    console.log(`User Social Media: ${this.state.user_socialMedia}`);

    const newUSer = {
      user_fullname: this.state.user_fullname,
      user_address: this.state.user_address,
      user_phonenumber: this.state.user_phonenumber,
      user_email: this.state.user_email,
      user_companyname: this.state.user_companyname,
      user_phone: this.state.user_phone,
      user_website: this.state.user_website,
      user_socialMedia: this.state.user_socialMedia,
      user_completed: this.state.user_completed
    };

    axios
      .post("http://localhost:4000/manager/add", newUSer)
      .then(res => console.log(res.data));

    this.setState({
      user_fullname: "",
      user_address: "",
      user_phonenumber: "",
      user_email: "",
      user_companyname: "",
      user_phone: "",
      user_website: "",
      user_socialMedia: "",
      user_completed: false
    });
  }

  render() {
    return (
      <div style={{ marginTop: 10, width: 700, paddingLeft: 300 }}>
        <h3>Create Customer Information</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Full name: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_fullname}
              onChange={this.onChangeUserFullname}
            />
          </div>
          <div className="form-group">
            <label>Address: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_address}
              onChange={this.onChangeUserAddress}
            />
          </div>
          <div className="form-group">
            <label>Phone Number: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_phonenumber}
              onChange={this.onChangeUserPhonenumber}
            />
          </div>

          <div className="form-group">
            <label>Email: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_email}
              onChange={this.onChangeUserEmail}
            />
          </div>
          <div className="form-group">
            <label>Comapany Name: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_companyname}
              onChange={this.onChangeUserCompanyname}
            />
          </div>
          <div className="form-group">
            <label>Phone: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_phone}
              onChange={this.onChangeUserPhone}
            />
          </div>
          <div className="form-group">
            <label>Website: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_website}
              onChange={this.onChangeUserWebsite}
            />
          </div>
          <div className="form-group">
            <label>Social media: </label>
            <input
              required
              type="text"
              className="form-control"
              value={this.state.user_socialMedia}
              onChange={this.onChangeUserSocialMedia}
            />
          </div>
          <div className="form-group">
            <input
              type="submit"
              value="Create User"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}
