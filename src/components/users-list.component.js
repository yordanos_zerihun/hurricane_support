import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

const User = props => (
  <tr>
    <td className={props.user.user_completed ? "completed" : ""}>
      {props.user.user_date}
    </td>
    <td className={props.user.user_completed ? "completed" : ""}>
      {props.user.user_fullname}
    </td>
    <td className={props.user.user_completed ? "completed" : ""}>
      {props.user.user_address}
    </td>
    <td className={props.user.user_completed ? "completed" : ""}>
      {props.user.user_phonenumber}
    </td>
    <td>
      <Link to={"/view/" + props.user._id}>More</Link>
    </td>
    <td>
      <Link to={"/edit/" + props.user._id}>Edit</Link>
    </td>
  </tr>
);

export default class UsersList extends Component {
  constructor(props) {
    super(props);
    this.state = { users: [], term: "" };
    this.onSearchHandler = this.onSearchHandler.bind(this);
  }

  componentDidMount() {
    axios
      .get("http://localhost:4000/manager/")
      .then(response => {
        this.setState({ users: response.data });
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  customerList() {
    const { users, term } = this.state;
    if (!term) {
      return users.map(function(currentUser, i) {
        return <User user={currentUser} key={i} />;
      });
    } else {
      return users
        .filter(user => {
          return user.user_fullname === term;
        })
        .map(matchingCustumer => {
          return <User user={matchingCustumer} />;
        });
    }
  }

  onSearchHandler(e) {
    this.setState({
      term: e.target.value
    });
  }

  render() {
    return (
      <div>
        <h3>Customers List</h3>
        <input
          type="text"
          className="form-control col-7"
          placeholder="Search Customer"
          onChange={this.onSearchHandler}
          value={this.state.user_fullname}
        ></input>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
            <tr>
              <th>Date</th>
              <th>Full name</th>
              <th>Address</th>
              <th>Phone number</th>
            </tr>
          </thead>
          {/* <tbody>{this.userList()}</tbody> */}
          <tbody>{this.customerList()}</tbody>
        </table>
      </div>
    );
  }
}
