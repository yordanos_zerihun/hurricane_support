import React, { Component } from "react";
import axios from "axios";
// import UsersList from "../components/users-list.component";
// import { Link } from "react-router-dom";

export default class ViewUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user_date: "",
      user_fullname: "",
      user_address: "",
      user_phonenumber: "",
      user_email: "",
      user_companyname: "",
      user_phone: "",
      user_website: "",
      user_socialMedia: "",
      user_completed: false
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:4000/manager/" + this.props.match.params.id)
      .then(response => {
        this.setState({
          user_date: response.data.user_date,
          user_fullname: response.data.user_fullname,
          user_address: response.data.user_address,
          user_phonenumber: response.data.user_phonenumber,
          user_email: response.data.user_email,
          user_companyname: response.data.user_companyname,
          user_phone: response.data.user_phone,
          user_website: response.data.user_website,
          user_socialMedia: response.data.user_socialMedia,
          user_completed: response.data.user_completed
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <h3 align="center">View User Information</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <ul className="list-group">
              <li className="list-group-item list-group-item-action">
                <b>Date:</b> {this.state.user_date}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Full Name:</b> {this.state.user_fullname}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Address:</b> {this.state.user_address}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Phone Number:</b> {this.state.user_phonenumber}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Email:</b> {this.state.user_email}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Company Name:</b> {this.state.user_companyname}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Phone:</b> {this.state.user_phone}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Website:</b> {this.state.user_website}
              </li>
              <li className="list-group-item list-group-item-action">
                <b>Social media:</b> {this.state.user_socialMedia}
              </li>
            </ul>
          </div>

          <br />
        </form>
      </div>
    );
  }
}
