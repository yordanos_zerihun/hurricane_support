import React, { Component } from "react";
import axios from "axios";

export default class EditUser extends Component {
  constructor(props) {
    super(props);
    this.onChangeUserDate = this.onChangeUserDate.bind(this);
    this.onChangeUserFullname = this.onChangeUserFullname.bind(this);
    this.onChangeUserAddress = this.onChangeUserAddress.bind(this);
    this.onChangeUserPhonenumber = this.onChangeUserPhonenumber.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onChangeUserCompanyname = this.onChangeUserCompanyname.bind(this);
    this.onChangeUserPhone = this.onChangeUserPhone.bind(this);
    this.onChangeUserWebsite = this.onChangeUserWebsite.bind(this);
    this.onChangeUserSocialMedia = this.onChangeUserSocialMedia.bind(this);
    this.onChangeUserCompleted = this.onChangeUserCompleted.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      user_date: "",
      user_fullname: "",
      user_address: "",
      user_phonenumber: "",
      user_email: "",
      user_companyname: "",
      user_phone: "",
      user_website: "",
      user_socialMedia: "",
      user_completed: false
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:4000/manager/" + this.props.match.params.id)
      .then(response => {
        this.setState({
          user_date: response.data.user_date,
          user_fullname: response.data.user_fullname,
          user_address: response.data.user_address,
          user_phonenumber: response.data.user_phonenumber,
          user_email: response.data.user_email,
          user_companyname: response.data.user_companyname,
          user_phone: response.data.user_phone,
          user_website: response.data.user_website,
          user_socialMedia: response.data.user_socialMedia,
          user_completed: response.data.user_completed
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  onChangeUserDate(e) {
    this.setState({
      user_date: e.target.value
    });
  }
  onChangeUserFullname(e) {
    this.setState({
      user_fullname: e.target.value
    });
  }
  onChangeUserAddress(e) {
    this.setState({
      user_address: e.target.value
    });
  }
  onChangeUserPhonenumber(e) {
    this.setState({
      user_phonenumber: e.target.value
    });
  }
  onChangeUserEmail(e) {
    this.setState({
      user_email: e.target.value
    });
  }
  onChangeUserCompanyname(e) {
    this.setState({
      user_companyname: e.target.value
    });
  }
  onChangeUserPhone(e) {
    this.setState({
      user_phone: e.target.value
    });
  }
  onChangeUserWebsite(e) {
    this.setState({
      user_website: e.target.value
    });
  }
  onChangeUserSocialMedia(e) {
    this.setState({
      user_socialMedia: e.target.value
    });
  }
  onChangeUserCompleted(e) {
    this.setState({
      user_completed: !this.state.user_completed
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const obj = {
      user_date: this.state.user_date,
      user_fullname: this.state.user_fullname,
      user_address: this.state.user_address,
      user_phonenumber: this.state.user_phonenumber,
      user_email: this.state.user_email,
      user_companyname: this.state.user_companyname,
      user_phone: this.state.user_phone,
      user_website: this.state.user_website,
      user_socialMedia: this.state.user_socialMedia,
      user_completed: this.state.user_completed
    };
    console.log(obj);
    axios
      .post(
        "http://localhost:4000/manager/update/" + this.props.match.params.id,
        obj
      )
      .then(res => console.log(res.data));

    this.props.history.push("/");
  }

  render() {
    return (
      <div style={{ marginTop: 10, width: 700, paddingLeft: 300 }}>
        <h3 align="center">Update User Information</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Date: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_date}
              onChange={this.onChangeUserDate}
            />
          </div>

          <div className="form-group">
            <label>Full name: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_fullname}
              onChange={this.onChangeUserFullname}
            />
          </div>
          <div className="form-group">
            <label>Address: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_address}
              onChange={this.onChangeUserAddress}
            />
          </div>
          <div className="form-group">
            <label>Phone Number: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_phonenumber}
              onChange={this.onChangeUserPhonenumber}
            />
          </div>

          <div className="form-group">
            <label>Email: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_email}
              onChange={this.onChangeUserEmail}
            />
          </div>
          <div className="form-group">
            <label>Comapany Name: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_companyname}
              onChange={this.onChangeUserCompanyname}
            />
          </div>
          <div className="form-group">
            <label>Phone: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_phone}
              onChange={this.onChangeUserPhone}
            />
          </div>
          <div className="form-group">
            <label>Website: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_website}
              onChange={this.onChangeUserWebsite}
            />
          </div>
          <div className="form-group">
            <label>Social media: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.user_socialMedia}
              onChange={this.onChangeUserSocialMedia}
            />
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              id="completedCheckbox"
              type="checkbox"
              name="completedCheckbox"
              onChange={this.onChangeUserCompleted}
              checked={this.state.user_completed}
              value={this.state.user_completed}
            />

            <label className="form-check-label" htmlFor="completedCheckbox">
              Communicated
            </label>
          </div>

          <br />

          <div className="form-group">
            <input
              type="submit"
              value="Update User"
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}

// export default EditUser;
